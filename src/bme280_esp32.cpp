#include "bme280_esp32.hpp"
#include <functional>
#include <thread>
#include <memory.h>
using namespace std;


BME280_ESP32::BME280_ESP32(gpio_num_t MISO, gpio_num_t MOSI, gpio_num_t SCLK, gpio_num_t CS, spi_host_device_t spiDev){
    espSpiInit(MISO,MOSI,SCLK,CS,spiDev);
    bmeInit();
}


void BME280_ESP32::bmeInit()
{
    device.intf=bme280_intf::BME280_SPI_INTF;
    device.read=BME280_ESP32::spi_read_wrapper;
    device.write=BME280_ESP32::spi_write_wrapper;
    device.delay_ms=BME280_ESP32::delayMs;
    device.ctx=this;
    device.settings.osr_h = BME280_OVERSAMPLING_8X;
    device.settings.osr_p = BME280_OVERSAMPLING_16X;
    device.settings.osr_t = BME280_OVERSAMPLING_8X;
    device.settings.filter = BME280_FILTER_COEFF_16;

    uint8_t settings_sel = BME280_OSR_PRESS_SEL | BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_FILTER_SEL;
    status=bme280_init(&device);
    if(BME280_OK==status){
        /* Set the sensor settings */
        status =bme280_set_sensor_settings(settings_sel, &device);
    }
    //the following two lines is requiered in order the validity of the first measurement.
    bme280_set_sensor_mode(BME280_FORCED_MODE, &device);
    this_thread::sleep_for(std::chrono::milliseconds(300));
}

void BME280_ESP32::espSpiInit(gpio_num_t MISO, gpio_num_t MOSI, gpio_num_t SCLK, gpio_num_t CS, spi_host_device_t spiDev){
    spi_bus_config_t buscfg={};
        buscfg.miso_io_num=MISO;
        buscfg.mosi_io_num=MOSI;
        buscfg.sclk_io_num=SCLK;
        buscfg.quadwp_io_num=-1;
        buscfg.quadhd_io_num=-1;
        buscfg.max_transfer_sz=0;
    spi_device_interface_config_t devcfg={};
        devcfg.clock_speed_hz=1'000'000;           //Clock out at 1 MHz
        devcfg.mode=0;                                //SPI mode 0
        devcfg.spics_io_num=CS;               //CS pin
        devcfg.input_delay_ns=20;
        devcfg.queue_size=7;
        devcfg.address_bits=8;
    //init spi bus
    esp_err_t ret=spi_bus_initialize(spiDev,&buscfg,2);
    ESP_ERROR_CHECK(ret);
    //set device config
    ret=spi_bus_add_device(spiDev, &devcfg, &spi);
    ESP_ERROR_CHECK(ret);

}

BME280_ESP32::MeasurementResult BME280_ESP32::getData(BME280_ESP32::ReadMode mode)
{
    MeasurementResult result={};
    bme280_set_sensor_mode(BME280_FORCED_MODE, &device);
    bme280_data data;
    status=bme280_get_sensor_data(static_cast<underlying_type<ReadMode>::type>(mode),&data,&device);
    if(BME280_OK==status){
        result.isValid=true;
        result.temperature=data.temperature;
        result.temperature*=0.01f;

        result.humidity=data.humidity;
        result.humidity*=0.001f;
        result.pressure=data.pressure*0.0001f;
    }
    return result;
}

int8_t BME280_ESP32::spi_read_wrapper(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len, void* ctx)
{
    spi_transaction_t t={};
    BME280_ESP32* m_ctx=reinterpret_cast<BME280_ESP32*>(ctx);

    t.rx_buffer=data;
    t.length=len*8;
    t.addr=reg_addr;
    spi_device_polling_transmit(m_ctx->spi,&t);
    return 0;
}

int8_t BME280_ESP32::spi_write_wrapper(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len, void* ctx)
{
    spi_transaction_t t={};
    BME280_ESP32* m_ctx=reinterpret_cast<BME280_ESP32*>(ctx);
    t.length=len*8;
    t.tx_buffer=data;
    t.addr=reg_addr;
    spi_device_polling_transmit(m_ctx->spi,&t);
    return 0;
}



void BME280_ESP32::delayMs(uint32_t period, void* ctx){
    this_thread::sleep_for(chrono::duration<uint32_t,std::milli>(period));
}

