#pragma once
#include <driver/gpio.h>
#include <driver/spi_master.h>
#include <bme280.h>
class BME280_ESP32{
public:
    typedef float BME280AtomicResultDataType;
    struct MeasurementResult{
        bool isValid;
        BME280AtomicResultDataType temperature;//Celsius
        BME280AtomicResultDataType humidity;//%
        BME280AtomicResultDataType pressure;//hPa
    };

    enum class ReadMode:uint8_t
    {
        M_BME280_PRESS=BME280_PRESS,
        M_BME280_TEMP=BME280_TEMP,
        M_BME280_HUM =BME280_HUM,
        M_BME280_ALL =BME280_ALL
    };
    BME280_ESP32(gpio_num_t MISO, gpio_num_t MOSI, gpio_num_t SCLK, gpio_num_t CS, spi_host_device_t spiDevice=spi_host_device_t::SPI1_HOST);
    MeasurementResult getData(ReadMode mode=ReadMode::M_BME280_ALL);

private:
    int8_t status;
    bme280_dev device;
    spi_device_handle_t spi;
    void bmeInit();
    void espSpiInit(gpio_num_t MISO, gpio_num_t MOSI, gpio_num_t SCLK, gpio_num_t CS, spi_host_device_t spiDev);
    void bmeSetupSensor();

    static int8_t spi_read_wrapper(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len, void* ctx);
    static int8_t spi_write_wrapper(uint8_t dev_id, uint8_t reg_addr, uint8_t *data, uint16_t len, void* ctx);
    static void delayMs(uint32_t period, void* ctx);
};
