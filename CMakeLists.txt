cmake_minimum_required (VERSION 3.13)
project(BME280_CPP)
include(FetchContent)
FetchContent_Declare(
  bme280driverlib
  GIT_REPOSITORY https://github.com/perimeno/BME280_driver.git
  GIT_TAG laci_v1.0
)
FetchContent_MakeAvailable(bme280driverlib)

file(GLOB srcs
    "src/*.hpp"
    "src/*.cpp"
)
add_library(bme280lib ${srcs})
target_include_directories(bme280lib INTERFACE "./src")
target_link_libraries(bme280lib bme280driverlib)
